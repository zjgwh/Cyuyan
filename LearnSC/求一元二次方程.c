/* 求一元二次方程 */
#include <stdio.h>
#include <math.h>

int main(void){
    float a,b,c,x1,x2,dt;//ax^2+bx+c //dt = b^2 - 4*a*c

    printf("pls input a, b, c (ax^2+bx+c):\n");
    scanf("%f %f %f", &a, &b, &c);
    dt = b*b - 4*a*c;
    if(dt > 0){
        x1 = ((-b) + sqrt(dt)) / (2*a);
        x2 = ((-b) - sqrt(dt)) / (2*a);
        printf("x1 = %.4f\n",x1);
        printf("x2 = %.4f",x2);
    }else if(dt == 0){
        x1 = ((-b) + sqrt(dt)) / (2*a);
        x2 = x1;
        printf("x1 = x2 = %.4f",x1);
    }else{
        printf("Error!");
    }

    return 0;
}
