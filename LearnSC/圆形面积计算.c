/* 圆形面积计算 */
#include <stdio.h>
#define PI 3.141592653589798

int main(void)
{
    int radius = 5; // int --- %d  半径  整数  %d %f 格式符
    float s,p;  //float --- %f  面积 周长 浮点数（实数）

    printf("please input r : "); //提示信息
    scanf("%d",&radius); //从键盘输值给r
    s = PI  * radius * radius;
    p = 2 * PI * radius;

    printf("r = %d\n",radius);
    printf("s = %f\n",s);
    printf("p = %f\n",p);
    printf("%d",sizeof(radius));
    return 0;
}
